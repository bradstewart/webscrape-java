# Use this file to specify input parameters for the web scraper.

# The SEARCH keyword tells the program which pre-defined websites to search.
# The FOR keyword tell the program what exactly to search the website for.
# CATEGORY is the Digikey category you wish to look for. Copy and paste this from Digikey;
#		it is case sensitive.

########### MCU LP INDIVIDUAL #############
#SEARCH:	Digikey
#FOR: EFM32WG; EFM32GG; EFM32LG; EFM32G; EFM32TG; EFM32ZG
#FILENAME: Energy Micro
#DIRECTORY: C:\LP_Price_Book\ 

#SEARCH:	Digikey
#FOR: C8051F9; SIM3L1
#FILENAME: SiliconLabs
#DIRECTORY: C:\LP_Price_Book\ 

#SEARCH:	Digikey
#FLAGS: 
#FOR: STM8L; STM32L1; LPC11; LPC12; LPC13
#FILENAME: ST_NXP
#DIRECTORY: C:\MCU_Schedule

#####################################################
############# SCHEDULER TESTING #####################
#####################################################

SEARCH:	Digikey
FLAGS: -S
FOR: LPC11; LPC12; LPC13
CATEGORY: Embedded - Microcontrollers
FILENAME: nxp
DIRECTORY: C:\MCU_Schedule\ 
END

#SEARCH: Digikey 
FLAGS:  -S
XLSX: C:\MCU_Schedule\\nxp_28-Oct-2013-152130.xlsx
SHEETNAME: 28-Oct-2013
CATEGORY: Embedded - Microcontrollers
FOR: LPC11; LPC12; LPC13
FILENAME: nxp
DIRECTORY: C:\MCU_Schedule\
END


#####################################################
######################## END ########################
#####################################################

#SEARCH:	Digikey
#FOR: AT32UC3L; AT91SAM7L; AT89LP
#FILENAME: Atmel
#DIRECTORY: C:\LP_Price_Book\ 

#SEARCH:	Digikey
#FOR: MKL0; MKL1; S08R; S08QB; MCF51MM; S08MM; S08LL; S08LH; 9S08QE128; MCF51QE128
#FILENAME: Freescale
#DIRECTORY: C:\LP_Price_Book\ 

#SEARCH:	Digikey
FLAGS: -P
FOR: microchip .and. xlp
FILENAME: Microchip
CATEGORY: Embedded - Microcontrollers
DIRECTORY: C:\LP_Price_Book\ 
END

#SEARCH:	Digikey
#FOR: MSP430G2; MSP430L092; MSP430C092; MSP430C091; MSP430FR; MSP430F1; MSP430AFE2; MSP430FW4; MSP430GW4; MSP430F4; MSP430CG4; MSP430C4; MSP430F5; MSP430F6
#FILENAME: TI
#DIRECTORY: C:\LP_Price_Book\ 

#SEARCH:	Digikey
FLAGS: -m
CATEGORY: Embedded - Microcontrollers
FOR: RL78; EFM32WG; EFM32GG; EFM32LG; EFM32G; EFM32TG; EFM32ZG; C8051F9; SIM3L1; STM8L; STM32L1; LPC11; LPC12; LPC13; AT32UC3L; AT91SAM7L; AT89LP; MKL0; MKL1; S08R; S08QB; MCF51MM; S08MM; S08LL; S08LH; 9S08QE128; MCF51QE128; MSP430G2; MSP430L092; MSP430C092; MSP430C091; MSP430FR; MSP430F1; MSP430AFE2; MSP430FW4; MSP430GW4; MSP430F4; MSP430CG4; MSP430C4; MSP430F5; MSP430F6
FILENAME: MCU
DIRECTORY: C:\LP_Price_Book\
END

########################################
################ TIMING ################
########################################
#SEARCH:	Digikey
FLAGS: -m
CATEGORY: Clock/Timing - Clock Buffers, Drivers; Clock/Timing - Clock Generators, PLLs, Frequency Synthesizers; Clock/Timing - Application Specific
FOR: Si53320-B-GT;Si53321-B-GM;Si53321-B-GQ;Si53325-B-GM;Si53325-B-GQ;Si53360-B-GT;Si53365-B-GT;Si53301-B-GM;Si53302-B-GM;Si53303-B-GM;Si53325-B-GQ;Si53360-B-GT;Si53301-B-GM;MAX9310;SY100EP14U;MC100LVEP14;MAX9311;MC100LVEP111MN;ICS85310I-21;ICS85310-1;ICS853S111BI;MAX9311;MC100LVEP111FA;CDCLVP111;MC100EP111;MC100ES6111;MC100LVEP111;MC100PTN111;MC100LVEP210MN;MC100LVEP210FA;CDCLVP215;MAX9312;ICS552G-02;ICS552-02;CDCLVC1108;ICS85105I;ICS853S014I;ICS8536I-33;ICS8536-01;ICS5T9306;ICS853S013I;ICS854S006I;ICS9DB633;ICS8SLVP1204ANLGI;ICS8538I-26;ICS85408I;ICS853S10I;ICS9DB833;ICS9DB823B;ICS8531-01;ICS853S031I;ICS83210;ICS85210-31;ICS85310I-11;ICS851010;ICS854110I;ICS5T907;ICS5T9310;ICS853S6111I;ICS854S036;ICS8SLVP1208ANBGI;ICS85310I-11;ICS5V2310;MAX9314;ICS8308I;DS92CK16
FILENAME: timing_competitors
DIRECTORY: C:\TEMP\
END
# ##### PROGRAMMABLE XO ##### #
#SEARCH:	Digikey
FLAGS: 
URL: http://www.digikey.com/scripts/dksearch/dksus.dll?pv71=60&pv71=419&pv71=953&pv71=293&pv71=84&pv71=81&FV=fff4000d%2Cfff8004f%2Cb83f2a%2Cb8434d%2C3f80029%2C3f8002a%2C3f8002f%2C3f80030%2C3f80031%2C3f80032%2C3f80037%2C3f80038%2C3f80040%2C3f80041%2C3f80042%2C3f80043%2C3f80047&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=0&ptm=0&fid=0&pageSize=25
CATEGORY: Programmable Oscillators
FOR: oscillator
FILENAME: Prog_XO
DIRECTORY: C:\TEMP\
END
# ##### STANDARD XO ##### #
#SEARCH: Digikey
FLAGS:
URL: http://www.digikey.com/scripts/dksearch/dksus.dll?pv183=6357&pv71=84&pv71=81&pv46=17229&pv46=16170&FV=fff4000d%2Cfff8016e&k=oscillator&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=0&ptm=0&fid=0&pageSize=25
CATEGORY: Oscillators
FOR: oscillator
FILENAME: Std_XO
DIRECTORY C:\TEMP\
END

########################################
############### SRW ####################
########################################
#SEARCH:	Digikey
FLAGS: 
CATEGORY: RF Transceivers
FOR: Si40;Si41;Si402;Si42;Si43;Si44
FILENAME: srw
DIRECTORY: C:\TEMP\
END

########################################
########### MCU BROAD BASED ############
########################################
#SEARCH: Digikey
FLAGS:
#URL: http://www.digikey.com/scripts/dksearch/dksus.dll?v=505&v=1050&v=313&FV=fff40027%2Cfff800cd%2C1c0000%2C1c0001%2C1c0002%2C1c0003%2C1c0006%2C1c0008%2C1c0011%2C26c009d%2C26c009e%2C26c009f%2C26c00a1%2C26c00a2%2C26c00a3%2C26c00ad%2C26c00ae%2C26c00af%2C26c00b3%2C26c00b4%2C26c00b6%2C26c00b7%2C26c00b8%2C26c00b9%2C26c00bc%2C26c00bd%2C26c00be%2C26c00c1%2C26c00c3%2C26c00c4%2C26c00c5%2C26c00cc%2C26c00cd%2C26c00d0%2C26c00d1%2C26c00d2%2C26c00d4%2C26c00d5%2C26c00d6%2C26c00d7%2C26c00da%2C26c00db%2C26c00dc%2C26c00df%2C26c00e0%2C26c00e1%2C26c00e2%2C26c00e3%2C26c00e4%2C26c00e9%2C26c00eb%2C26c00ec%2C26c00ee%2C26c00ef%2C26c00f0%2C26c00f1%2C26c00f2%2C26c00f3%2C26c00f4%2C26c00f7%2C26c00f8%2C26c00f9%2C26c00fc%2C26c00fd%2C26c00fe%2C26c0101%2C26c0102%2C26c0103%2C26c010d%2C26c010e%2C26c0110%2C26c0111%2C26c0112%2C26c0113%2C26c0116%2C26c0117%2C26c0118%2C26c0119%2C26c011a%2C26c011d%2C26c011e%2C26c011f%2C26c0121%2C26c0122%2C26c0126%2C26c0127%2C26c0129%2C26c012a%2C26c012b%2C26c012d%2C26c0139%2C26c013a%2C26c013d&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=100&ptm=0&fid=0&pageSize=25
#URL: http://www.digikey.com/scripts/dksearch/dksus.dll?pv155=188&pv155=189&pv155=190&pv155=274&pv155=157&pv155=158&pv155=159&pv155=161&pv155=162&pv155=275&pv155=204&pv155=205&pv155=270&pv155=218&pv155=208&pv155=209&pv155=210&pv155=294&pv155=226&pv155=227&pv155=228&pv155=219&pv155=238&pv155=278&pv155=247&pv155=248&pv155=252&pv155=249&pv155=295&pv155=257&pv155=258&pv155=259&pv155=313&pv155=163&pv155=173&pv155=174&pv155=279&pv155=175&pv155=179&pv155=180&pv155=182&pv155=183&pv155=272&pv155=287&pv155=184&pv155=185&pv155=297&pv155=193&pv155=195&pv155=196&pv155=197&pv155=280&pv155=269&pv155=317&pv155=289&pv155=212&pv155=213&pv155=214&pv155=215&pv155=286&pv155=220&pv155=281&pv155=223&pv155=224&pv155=225&pv155=233&pv155=235&pv155=285&pv155=236&pv155=239&pv155=240&pv155=314&pv155=241&pv155=273&pv155=290&pv155=242&pv155=243&pv155=244&pv155=282&pv155=298&pv155=299&pv155=301&pv155=253&pv155=254&FV=fff40027%2Cfff800cd&mnonly=0&newproducts=0&ColumnSort=0&page=1&quantity=0&ptm=0&fid=0&pageSize=250
URL: http://www.digikey.com/product-search/en/integrated-circuits-ics/embedded-microcontrollers/2556109?k=microcontroller
CATEGORY: Embedded - Microcontrollers
FOR: microcontroller
FILENAME: MCU_All
DIRECTORY: C:\MCU_Broad_Based_Data\
END

# #######################################
# ########### APS ISOLATORS #############
# #######################################
#SEARCH: Digikey
FLAGS: 
CATEGORY: Digital Isolators; Optoisolators - Logic Output; Optoisolators - Transistor, Photovoltaic Output
FOR: Si871;Si8261;Si8232BB-B-IS;6N137;6N138;6N139;ACNV3130-000E;ACNW261L;ACNW3130-000E;ACNW3190-000E;ACPL-061L;ACPL-312U-000E;ACPL-3130-000E;ACPL-H312-000E;ACPL-J313-000E;ACPL-K312-000E;ACPL-P3;ACPL-P611;ACPL-T350-000E;ACPL-W3;ACPL-W6;HCNW13;HCNW22;HCNW26;HCNW3120;HCNW450;HCPL-02;HCPL-03;HCPL-04;HCPL-06;HCPL-070;HCPL-22;HCPL-2300;HCPL-2601;HCPL-261;HCPL-3020;HCPL-31;HCPL-4;HCPL-J3;HCPL-T2
FILENAME: APS
DIRECTORY: C:\TEMP\
END

#SEARCH: Digikey
FLAGS:
CATEGORY: Interface - Controllers
FOR: CP2102;CP2109;CP2104;CP2105;CP2108;CP2110;FT230;FT231;FT234XD;FT232R;FT2232D;FT2232H;FT4232H;MCP2200;CY7C64225;XR21V1410;USB-232
FILENAME: FixedFunction
DIRECTORY: C:\users\brstewar\desktop
END
