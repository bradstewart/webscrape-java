/**
 * SearchParameters.java
 *
 * Brad Stewart
 * Silicon Laboratorties, Inc.
 * MCU Marketing
 * 
 */
package com.silabs.mcu.webscrape.io;

/**
 * @author Brad Stewart (brstewar) * 
 * Data container class for search parameters. Values are set by InputProcessor
 *  and read by DIGIKEY or another search provider.
 *
 */
public class SearchParameters {
	
	private String paramFilename = null; // Remember param file name for scheduling
	private String criteria = null; // Search terms: part families, or parameters
	private String category = null; // Digikey category link text NOTE: Case sensitive
	private String filename = null; // Output file name
	private String xlsx = null; // Path to XSLX file if writing a new tab
	private String oldSheetName = null; // Name of sheet within XLS containing old data
	private String directory = null; // Output directory
	private String parametricURL = null;
	private boolean beQuiet = false; // True = Don't print status updates, will still print error information
	private boolean checkMouser = false; // True = Also search Mouser.com
	private boolean doParametric = false; // True = parametric search (AND) instead of family search (OR)
	private boolean isScheduled = false; // True = generate param.txt for future runs
	
	
	public SearchParameters() { }

	public String getCriteria() {
		return criteria;
	}

	public SearchParameters setCriteria(String criteria) {
		this.criteria = criteria;
		return this;
	}

	public String getFilename() {
		return filename;
	}

	public SearchParameters setFilename(String filename) {
		this.filename = filename;
		return this;
	}

	public String getDirectory() {
		return directory;
	}

	public SearchParameters setDirectory(String directory) {
		this.directory = directory;
		return this;
	}


	public boolean beQuiet() {
		return beQuiet;
	}

	public SearchParameters setQuiet(boolean beQuiet) {
		this.beQuiet = beQuiet;
		return this;
	}

	public boolean checkMouser() {
		return checkMouser;
	}

	public SearchParameters setMouser(boolean checkMouser) {
		this.checkMouser = checkMouser;
		return this;
	}

	public boolean isParametric() {
		return doParametric;
	}

	public SearchParameters setParametric(boolean doParametric) {
		this.doParametric = doParametric;
		return this;
	}

	public String getCategory() {
		return category;
	}

	public SearchParameters setCategory(String category) {
		this.category = category;
		return this;
	}

	public String getXlsx() {
		return xlsx;
	}

	public void setXlsx(String xlsx) {
		this.xlsx = xlsx;
	}

	public String getOldSheetName() {
		return oldSheetName;
	}

	public void setOldSheetName(String oldSheetName) {
		this.oldSheetName = oldSheetName;
	}

	public boolean isScheduled() {
		return isScheduled;
	}

	public void setScheduled(boolean isScheduled) {
		this.isScheduled = isScheduled;
	}

	public String getParametricURL() {
		return parametricURL;
	}

	public void setParametricURL(String parametricURL) {
		this.parametricURL = parametricURL;
	}

	public String getParamFilename() {
		return paramFilename;
	}

	public void setParamFilename(String paramFilename) {
		this.paramFilename = paramFilename;
	}

}
