/**
 * InputProcessor.java
 *
 * Brad Stewart
 * Silicon Laboratorties, Inc.
 * MCU Marketing
 * 
 */
package com.silabs.mcu.webscrape.io;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

/**
 * @author Brad Stewart (brstewar)
 * Reads and parses the options present in the params.txt file and
 *  launches the search application. *  
 * Input parameters are explained in ReadMe.txt
 * Will work for any WebScraper interface.
 *
 */
public class InputProcessor {
	public static void main(String[] args) {
		
//		Table<String, String, String> test = HashBasedTable.create();
//		test.put("MSP430", "Manufacturer Part Number", "MSP430");
//		test.put("MSP430", "Manufacturer", "Texas Instruments");
//		test.put("MSP430", "NEW: Digikey Price @100", "1");
//		test.put("MSP430", "NEW: Digikey Price @1000", "11");
//		test.put("MSP430", "NEW: Digikey Price @10000", "111");
//		test.put("MSP430", "OLD: Digikey Price @100", "2");
//		test.put("MSP430", "OLD: Digikey Price @1000", "22");
//		test.put("MSP430", "OLD: Digikey Price @10000", "222");
//		test.put("F990", "Manufacturer Part Number", "F990");
//		test.put("F990", "Manufacturer", "Silicon Labs");
//		test.put("F990", "NEW: Digikey Price @100", "3");
//		test.put("F990", "NEW: Digikey Price @1000", "33");
//		test.put("F990", "NEW: Digikey Price @10000", "333");
//		test.put("F990", "OLD: Digikey Price @100", "4");
//		test.put("F990", "OLD: Digikey Price @1000", "44");
//		test.put("F990", "OLD: Digikey Price @10000", "444");
//		
//		SearchParameters sp = new SearchParameters();
//		sp.setMouser(true).setFilename("TEST_NEW_TABLE").setDirectory("C:\\TEMP\\");
//		
//		ExcelFile xls = new ExcelFile(sp);
//		xls.writePriceDiffTable(test, sp, "test");
//		xls.writeFile();
		
		
		System.out.print("Processing input...");
			try {
				File params = new File(args[0]);				  
				processInput( params );				
			} catch (IOException e) {
				System.out.println("Parameter input file not found.");
				System.out.println("Please include a valid parameter as an argument, or verify the location of the file.");
			}	
	}
	
	private static void processInput( File paramFile ) throws IOException {
		LineIterator it = FileUtils.lineIterator( paramFile );
		String paramFilename = paramFile.getName();
		
		while ( it.hasNext() ) {
			String line = it.nextLine();
			line = line.trim();
			line = line.toUpperCase();
			
			if( line.startsWith("#")) { // Comment line
				continue;
				
			} else if( line.startsWith("SEARCH") ) {
				
				String search = processString(line);
				SearchParameters sp = new SearchParameters();
				sp.setParamFilename( paramFilename );
				String params = null;			
				
				/**
				 *  Use reflection to instantiate and the search provider object 
				 *  and find its initialize() and scrape() methods.
				 */
				Class<?> c = null;
					try {
						c = Class.forName("com.silabs.mcu.webscrape."+search);
					} catch (ClassNotFoundException e1) {
						System.out.println("Invalid search provider.");
						e1.printStackTrace();
					}
					
				Object obj = null;
					try {
						obj = c.newInstance();
					} catch (InstantiationException | IllegalAccessException e1) {
						System.out.println("Could not instantiate a search class.");
						e1.printStackTrace();
					}
				
				Method init = null;
					try {
						init = c.getDeclaredMethod("initialize", SearchParameters.class);
					} catch (NoSuchMethodException | SecurityException e1) {
						System.out.println("Failed to initialize search parameters.");
						e1.printStackTrace();
					}
					
				Method srch = null;
					try {
						srch = c.getDeclaredMethod("scrape");
					} catch (NoSuchMethodException | SecurityException e1) {
						System.out.println("Faield to find scrape() method.");
						e1.printStackTrace();
					}
					
					
				/**
				 * Interrogate the input file for keywords,
				 *  and populate the SearchParameters object.	
				 */
				while ( it.hasNext() ) {					
					line = it.nextLine();
					line = line.trim();
					
					if (line.startsWith("#")) {
						continue;
					} if (line.startsWith("END")) {
						break;
					} if (line.startsWith("FOR")) {
						params = processString(line);
						sp.setCriteria(params);						
					} if (line.startsWith("FILENAME")) {
						params = processString(line);
						sp.setFilename(params);
					} if (line.startsWith("DIRECTORY")) {
						params = processString(line);
						sp.setDirectory(params);
					} if (line.startsWith("CATEGORY")) {
						params = processString(line);
						sp.setCategory(params);						
					} if (line.startsWith("XLSX")) {
						params = processString(line);
						sp.setXlsx(params);						
					} if (line.startsWith("SHEETNAME")) {
						params = processString(line);
						sp.setOldSheetName(params);						
					} if (line.startsWith("URL")) {
						params = processString(line);
						sp.setParametricURL(params);	
					} if (line.startsWith("FLAGS")) {
						params = processString(line);
						params = params.toUpperCase();
						
						String[] flags = params.split("-");
						
						for ( String f : flags ) {
							f = f.trim();
							if ( f.equals("Q") || f.equals("QUIET") ) {
								sp.setQuiet(true);
							} else if ( f.equals("M") || f.equals("MOUSER") ) {
								sp.setMouser(true);							
							} else if ( f.equals("P") || f.equals("PARAMETRIC") ) {
								sp.setParametric(true);
							} else if ( f.equals("S") || f.equals("SCHEDULED") ) {
								sp.setScheduled(true);
							}
						}
					}
				}
				
				if ( sp.getCriteria() == null && sp.getParametricURL() == null ) { System.out.println("Bad Input: No search criteria."); return; }
				if ( sp.getCategory() == null && sp.getParametricURL() == null ) { System.out.println("Bad Input: No category."); return; }
				if ( sp.getFilename() == null ) { System.out.println("Bad Input: No output file defined."); return; }
		
				
				System.out.println(" Done.");
				
				/**
				 * Use reflection to call the initialize() and scrape() methods.
				 * This actually runs the search.
				 */
				try {
					init.invoke(obj, sp);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					System.out.println("Failed to initialize");
					e.printStackTrace();
				}
				try {
					srch.invoke(obj);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
				}
			}
					
		}
	}

	/**
	 * Parses a line of the input file. 
	 * Anything after a colon (":") is treated as input an parameter.
	 * @param line
	 * @return
	 */
	public static String processString(String line) {
		String params;
		params = line.substring( line.indexOf(":")+1 );
		params = params.trim();
		return params;
	}
}
