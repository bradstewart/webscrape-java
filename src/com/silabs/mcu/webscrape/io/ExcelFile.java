/**
 * ExcelFile.java
 *
 * Brad Stewart
 * Silicon Laboratorties, Inc.
 * MCU Marketing
 * 
 */
package com.silabs.mcu.webscrape.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.common.collect.Table;
import com.silabs.mcu.webscrape.Util;

/**
 * @author Brad Stewart (brstewar)
 * Processes data table, writes data to an Excel File object,
 *  and writes the file to disk.
 */
@SuppressWarnings("deprecation")
public class ExcelFile {
	private DateFormat dateFormat;
	private Date date;
	private Workbook workbook;
	private Map<String, CellStyle> styles;
	private SearchParameters sp;
	
	/**
	 * Constructor.
	 * @param sp			SearchParameters 
	 */

	public ExcelFile(SearchParameters sp) {		
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy-HHmmss"); // Date for the timestamp
	    date = new Date();
	    if ( sp.getXlsx() != null ) {
	    	InputStream inp; 
			try { // Attempt to open specified Excel file
				inp = new FileInputStream(sp.getXlsx());
					try {
						setWorkbook(WorkbookFactory.create(inp));
					} catch (InvalidFormatException | IOException e) {
						System.out.println("XLS(X) file could not be read. Invalid format.");
						System.out.println("Results will be collected and written to a new file.");
						sp.setXlsx(null); // Invalid format, so ignore it and make a new one.
						setWorkbook(new XSSFWorkbook());
					}
			} catch (FileNotFoundException e1) {
				System.out.println("XLS(X) file could not be found.");
				System.out.println("Results will be collected and written to a new file.");
				sp.setXlsx(null);
				setWorkbook(new XSSFWorkbook());
			}
	    	
	    } else { // No input file supplied so make a new one
	    	setWorkbook(new XSSFWorkbook());
	    }
		styles = createStyles(getWorkbook());
		this.sp = sp;
	}
	
	
	/**
	 * Takes a data Table from the web scrape code, creates a new sheet in the Excel object,
	 *  and writes the data to the sheet in the proper format.
	 * This method does NOT write anything to disk.
	 * @param table		Table to be written to Excel format
	 * @param sp		SearchParameters 
	 * @param sheetName Name for the new sheet
	 */
	public String writeData(Table<String,String,String> table, SearchParameters sp, String sheetName) {
		DateFormat df = new SimpleDateFormat("HH-mm-ss");
		Sheet sheet = null;
		String newName = sheetName;
		try {
			sheet = workbook.createSheet(sheetName);
		} catch ( IllegalArgumentException e ) {
			newName = sheetName+"_"+df.format(date);
			sheet = workbook.createSheet(newName);
		}

		ArrayList<String> columnHeaders = new ArrayList<String>();
		columnHeaders.addAll(table.columnKeySet()); // Put it in a list so we can control the order
		Row headerRow = sheet.createRow(0); // Make the header row in the Excel sheet
        headerRow.setHeightInPoints(12.75f); // Make the header row bigger
        
        Collections.sort(columnHeaders); // Sort the columns so the prices go together
        try {
	        Collections.swap(columnHeaders, columnHeaders.indexOf("Manufacturer Part Number"), 0);  // First column  
	        Collections.swap(columnHeaders, columnHeaders.indexOf("Manufacturer"), 1); // Second column
	        Collections.swap(columnHeaders, columnHeaders.indexOf("Description"), 2); // Third column
        } catch (Exception e) { /* OK to ignore if one of these fields is not present in the data */ }
        
        for ( int i = 0; i < columnHeaders.size(); i++ ) {
             Cell cell = headerRow.createCell(i); // Create a new cell
             cell.setCellValue(columnHeaders.get(i)); // Write the value to it
             cell.setCellStyle(styles.get("header")); // Style it
        }  
              
        sheet.createFreezePane(1, 1); //freeze the first row and column
        int rowNum = 1;
		int cellNum = 0;		

		Set<String> partNumbers = table.rowKeySet();
		for(String rowPartNumber : partNumbers) {
			
			if (!rowPartNumber.equals("Header")) {				
			Row row = sheet.createRow(rowNum++);
	
				cellNum = 0;
				for( String columnHeader : columnHeaders ) {
					String attribute = table.get(rowPartNumber, columnHeader);
					CellStyle style = null;
					try{
					style = table.get(rowPartNumber, "Manufacturer").contains("Silicon Lab") ? styles.get("cell_bg") : styles.get("cell_normal"); // Make the SiLabs parts stand out
					} catch (Exception e) { continue;  }
					/* Make the price attributes appear as numbers in Excel */
					if ( columnHeader.contains("Price") && attribute != null ) {				
						Double price = 0.00;
						try {
							price = Double.parseDouble( attribute );
						} catch (Exception e) { /* OK to ignore */ }					
						row.createCell(cellNum).setCellValue(price);
						row.getCell(cellNum++).setCellStyle(style);
					} else {
						row.createCell(cellNum).setCellValue(attribute);
						row.getCell(cellNum++).setCellStyle(style);
					}
				}
			}
		}
		
		setAutoFilter(sheet);		
		autoSizeColumns(sheet);
		return newName;
	}
	
	/**
	 * Writes the data from Util.findPriceDiffs into a more readable format
	 *  than the standard table layout above.
	 */
	public void writePriceDiffTable(Table<String,String,String> table, SearchParameters sp, String sheetName) {
		  	CellStyle topBorder = workbook.createCellStyle();
			topBorder.setBorderTop(CellStyle.BORDER_THIN);
			CellStyle bottomBorder = workbook.createCellStyle();
			bottomBorder.setBorderBottom(CellStyle.BORDER_THIN);
			CellStyle rightBorder = workbook.createCellStyle();
			rightBorder.setBorderRight(CellStyle.BORDER_THIN);
			CellStyle topRightBorder = workbook.createCellStyle();
			topRightBorder.setBorderRight(CellStyle.BORDER_THIN);
			topRightBorder.setBorderTop(CellStyle.BORDER_THIN);
			
			if ( workbook.getSheet(sheetName) != null ) {
				workbook.removeSheetAt( workbook.getSheetIndex(sheetName) );
			}
			Sheet sheet = getWorkbook().createSheet(sheetName);
			ArrayList<String> columnHeaders = new ArrayList<String>();
			columnHeaders.addAll(table.columnKeySet()); // Put it in a list so we can control the order
			int rowNum = 0, cellNum = 0;
			Row distributorRow = sheet.createRow(rowNum++); // Make the top row for Digikey/Mouser
				Cell digikey = distributorRow.createCell(3); 
				distributorRow.createCell(4);
				digikey.setCellValue("Digikey");
				digikey.setCellStyle(styles.get("cell_column_header"));
				sheet.addMergedRegion( new CellRangeAddress(0,0,3,4) ); // Merge horizontal cells column headers
			Row dateRow = sheet.createRow(rowNum++); // Make a second row for Current/Previous
			Cell cell = dateRow.createCell(2);
				cell.setCellValue("Qty");
				cell.setCellStyle(rightBorder);
			cell = dateRow.createCell(3);
			cell.setCellValue("Current");
				cell.setCellStyle(bottomBorder);
			cell = dateRow.createCell(4);
				cell.setCellValue("Previous");
				cell.setCellStyle(bottomBorder);
			
				
			if ( sp.checkMouser() ) { // If we ran Mouser data, include the header for that as well
				Cell mouser = distributorRow.createCell(5); distributorRow.createCell(6);
				mouser.setCellValue("Mouser");
				
				sheet.addMergedRegion( new CellRangeAddress(0,0,5,6) );
				mouser.setCellStyle(styles.get("cell_column_header"));
				cell = dateRow.createCell(5);
				cell.setCellValue("Current");
					cell.setCellStyle(bottomBorder);
				cell = dateRow.createCell(6);
					cell.setCellValue("Previous");
					cell.setCellStyle(bottomBorder);
			}
			
	        Collections.sort(columnHeaders); // Sort the columns so the prices go together
	        try {
		        Collections.swap(columnHeaders, columnHeaders.indexOf("Manufacturer Part Number"), 0);  // First column  
		        Collections.swap(columnHeaders, columnHeaders.indexOf("Manufacturer"), 1); // Second column
	        } catch (Exception e) { /* OK to ignore if one of these fields is not present in the data */ }
	        
	        Set<String> partNumbers = table.rowKeySet();      
			
			for(String partNumber : partNumbers) {
				
				if (!partNumber.equals("Header")) {	
					cellNum = 0;
					int priceRowNum = rowNum;

					Row hundredRow = sheet.createRow(rowNum++); // Create row for the @100 pricing
						cell = hundredRow.createCell(2);
								cell.setCellValue("100");
								cell.setCellStyle(topRightBorder);
					Row thousandRow = sheet.createRow(rowNum++); // Create row for the @1,000 pricing
						cell = thousandRow.createCell(2);
						cell.setCellValue("1,000");
						cell.setCellStyle(rightBorder);
					Row tenThousandRow = sheet.createRow(rowNum++); // Create row for the @10,000 pricing
						cell = tenThousandRow.createCell(2);
						cell.setCellValue("10,000");
						cell.setCellStyle(rightBorder);
						
					cell = hundredRow.createCell(0); // Add the part information, using the hundredRow because creating a new row erases any previous row at the same index
						cell.setCellValue( table.get(partNumber, "Manufacturer Part Number") );
						
						sheet.addMergedRegion( new CellRangeAddress(priceRowNum,priceRowNum+2,0,0) ); // Merge vertical cells for part number
						cell.setCellStyle( styles.get("cell_row_header") );
					cell = hundredRow.createCell(1);
						cell.setCellValue( table.get(partNumber, "Manufacturer") );
						
						sheet.addMergedRegion( new CellRangeAddress(priceRowNum,priceRowNum+2,1,1) ); // Merge vertical cells for manufacturer
						cell.setCellStyle( styles.get("cell_row_header") );
						
					
					for( String columnHeader : columnHeaders ) {	
						
						String stringValue = table.get(partNumber, columnHeader);
						boolean hasValue = true;
						
						/* Ensure only numerical values get written to the sheet
						   If stringValue is a null, then that field was not changed since previous-run data,
						    we want to prevent a 0 from being written, so that's what hasValue is for.
						   Yes, its a dumb way to do it. 
						*/
						try { Double.parseDouble(stringValue); }
						catch (Exception e){ stringValue = "0.0"; hasValue = false; }
						
						
						if ( columnHeader.equals("NEW Price: Digikey @100") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = hundredRow.createCell(3);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("NEW Price: Digikey @1000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = thousandRow.createCell(3);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("NEW Price: Digikey @10000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = tenThousandRow.createCell(3);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("OLD Price: Digikey @100") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = hundredRow.createCell(4);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("OLD Price: Digikey @1000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = thousandRow.createCell(4);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("OLD Price: Digikey @10000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = tenThousandRow.createCell(4);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("NEW Price: Mouser @100") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = hundredRow.createCell(5);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("NEW Price: Mouser @1000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = thousandRow.createCell(5);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("NEW Price: Mouser @10000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = tenThousandRow.createCell(5);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("OLD Price: Mouser @100") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = hundredRow.createCell(6);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("OLD Price: Mouser @1000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = thousandRow.createCell(6);
							cell.setCellValue(numValue);
						} else if ( columnHeader.equals("OLD Price: Mouser @10000") && hasValue ) {
							Double numValue = Double.valueOf( stringValue );
							cell = tenThousandRow.createCell(6);
							cell.setCellValue(numValue);
						} 
						
					}
					
				}
				
			}
			
			autoSizeColumns(sheet);
	}
	
	public int getSheetNumerOfRows(String sheetname) {
		return workbook.getSheet(sheetname).getLastRowNum();
	}
	
	/**
	 * Writes the Excel File object to file, sets up the table filters, and auto sizes the columns.
	 */
	public String writeFile() {
		if ( !sp.beQuiet() ) { System.out.println("Search complete. Writing Excel file..."); }
//		for (int i=0; i < workbook.getNumberOfSheets(); i++) {
//			
//			Sheet sheet = workbook.getSheetAt(i);
//			
//			setAutoFilter(sheet);
//			
//			autoSizeColumns(sheet);
//		}
		 
		 // Find or create the specified directory
		 String directory = sp.getDirectory().concat("\\");
		 File file = new File(directory);
		 try{
		 	if(file.exists()) {	}
		 	else if(file.mkdirs()) { System.out.println("Directory Created"); } 
		 	else {
		        System.out.println("Directory not created, using C:\\");
		        directory = "C:\\";
		    }
		} catch(Exception e){
		    e.printStackTrace();
		} 
		 
		 String fileEnding = null; // Write the proper file ending depending on workbook format
		 if ( workbook instanceof XSSFWorkbook) {
			 fileEnding = ".xlsx";
		 } else if ( workbook instanceof HSSFWorkbook) {
			 fileEnding = ".xls";
		 }
		
		// Write the file
		 String outputFile = directory+sp.getFilename()+"_"+dateFormat.format(date)+fileEnding;

		 try {
		    FileOutputStream out = 
		            new FileOutputStream(new File(outputFile));
		    getWorkbook().write(out);
		    out.close();
		    System.out.println("Excel written successfully.");
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		 
//		 if ( sp.isScheduled() ) {
//			 Util.generateParamFile(sp, outputFile, new SimpleDateFormat("dd-MMM-yy").format(date));
//		 }
		 
		 return outputFile;
	}

	/**
	 * @param sheet
	 */
	private void autoSizeColumns(Sheet sheet) {
		Properties prop = java.lang.System.getProperties(); // Do not autosize columns if Java version 1.7_21 due to TextLayout bug			
		if ( !prop.getProperty("java.version").contains("1.7.0_21") ) { // Don't try it if bad version of Java
			for (int j = 0; j < sheet.getRow(0).getPhysicalNumberOfCells(); j++) { // Autosize the columns so you can read them
		         sheet.autoSizeColumn(j, true);
		     }
		}
	}

	/**
	 * @param sheet
	 */
	private void setAutoFilter(Sheet sheet) {
		Row lastRow = sheet.getRow(sheet.getLastRowNum());			
		Row firstRow = sheet.getRow(0);
		Cell firstCell = firstRow.getCell( 0 );			
		Cell lastCell = lastRow.getCell( (int) lastRow.getLastCellNum()-1 ); // Save the last cell for the filter area
		sheet.setAutoFilter( new CellRangeAddress(firstCell.getRowIndex(), lastCell.getRowIndex(), firstCell.getColumnIndex(), lastCell.getColumnIndex()) ); // Define filter area
	}

	
	/**
	 * Source: Apache POI Getting Started Examples
     * create a library of cell styles
     */
    private static Map<String, CellStyle> createStyles(Workbook wb){
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        DataFormat df = wb.createDataFormat();

        CellStyle style;
        Font headerFont = wb.createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        
        Font font1 = wb.createFont();
        font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
        
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(headerFont);
        styles.put("header", style);
        
        style = createBorderedStyle(wb);
        style.setFont(font1);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("cell_bg", style);
        
        style = createBorderedStyle(wb);
        styles.put("cell_normal", style);
        
        style = createRowBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        styles.put("cell_row_header", style);
        
        style = createColumnBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font1);
        styles.put("cell_column_header", style);
        
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font1);
        styles.put("cell_b", style);
        
        /**
         * Unused but saved for future use.
         */

        
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(headerFont);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("header_date", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font1);
        styles.put("cell_b_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(font1);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_b_date", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(font1);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_g", style);

        Font font2 = wb.createFont();
        font2.setColor(IndexedColors.BLUE.getIndex());
        font2.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(font2);
        styles.put("cell_bb", style);

        Font font3 = wb.createFont();
        font3.setFontHeightInPoints((short)14);
        font3.setColor(IndexedColors.DARK_BLUE.getIndex());
        font3.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(font3);
        style.setWrapText(true);
        styles.put("cell_h", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setWrapText(true);
        styles.put("cell_normal_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setWrapText(true);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_normal_date", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setIndention((short)1);
        style.setWrapText(true);
        styles.put("cell_indented", style);

        style = createBorderedStyle(wb);
        style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("cell_blue", style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }
    
    private static CellStyle createRowBorderedStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
//        style.setBorderRight(CellStyle.BORDER_THIN);
//        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }
    
    private static CellStyle createColumnBorderedStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }


	public Workbook getWorkbook() { // Access to the workbook for Util methods
		return workbook;
	}


	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public SearchParameters getSp() {
		return sp;
	}

	public void setSp(SearchParameters sp) {
		this.sp = sp;
	}
    
}
