/**
 * ${file_name}
 *
 * Silicon Laboratorties, Inc.
 * MCU Marketing
 * Last Modified: ${date}${time}
 */
package com.silabs.mcu.webscrape.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @author brstewar
 * Static methods for writing the search results to a Microsoft Excel file. *
 */
public class ExcelOutput {
	/**
	 * Static method to process search results and write the output file.
	 * @param header		List<String> of data column headers
	 * @param map			Map<String>, List<String>> of search result data
	 * @param sp			SearchParameters
	 */
	public static void createFile(List<String> header, Map<String, List<String>> map, SearchParameters sp) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HHmmss"); // Date for the timestamp
	    Date date = new Date();		
		HSSFWorkbook workbook = new HSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(workbook);
		HSSFSheet sheet = workbook.createSheet(sp.getFilename());		
				
		Row headerRow = sheet.createRow(0);
        headerRow.setHeightInPoints(12.75f); // Make the header row bigger
        
        int priceColStart = 0, priceColEnd = 0;
        
        for ( int i = 0; i<header.size(); i++ ) {
        	if ( header.get(i).contains("Price") && priceColStart == 0 ) { priceColStart = i; } // Remember the first price column so we can make it a number later
        	if ( header.get(i).contains("Price") ) { priceColEnd = i; } // Remember the last price column so we don't make something else a number
             Cell cell = headerRow.createCell(i);
             cell.setCellValue(header.get(i));
             cell.setCellStyle(styles.get("header")); // Style the header row
        }       
        
        sheet.createFreezePane(1, 1); //freeze the first row and column

        int rowNum = 1;
        int cellNum = 0;	
        int priceCol = priceColStart;
        Set<String> parts = map.keySet();
		
		for ( String partNumber : parts ) {
			Row row = sheet.createRow(rowNum++);
			List<String> attributes = map.get(partNumber);
			cellNum = 0;
			priceCol = priceColStart;
			for( String attribute : attributes ) {
				// Make the price attributes appear as numbers in Excel
				// If the price columns are not contiguous, something weird will happen
				if ( cellNum == priceCol ) { 
					Double price = (attribute != null) ? Double.parseDouble( attribute ) : 0.00;
					row.createCell(cellNum++).setCellValue(price);
//					if ( priceCol <= priceColEnd) { priceCol++; }
					priceCol++;
				} else {
					row.createCell(cellNum++).setCellValue( attribute );
				}
			}
		}
		
		// Autosize the columns so you can read them
		 for (int i = 0; i < header.size(); i++) {
	         sheet.autoSizeColumn(i);
	     }
		 
		 // Find or create the specified directory
		 String directory = sp.getDirectory().concat("\\");
		 File file = new File(directory);
		 try{
		 	if(file.exists()) {	}
		 	else if(file.mkdirs()) { System.out.println("Directory Created"); } 
		 	else {
		        System.out.println("Directory not created, using C:\\");
		        directory = "C:\\";
		    }
		} catch(Exception e){
		    e.printStackTrace();
		} 
		
		// Write the file
		 try {
		    FileOutputStream out = 
		            new FileOutputStream(new File(directory+sp.getFilename()+"_"+dateFormat.format(date)+".xls"));
		    workbook.write(out);
		    out.close();
		    if ( !sp.beQuiet() ) { System.out.println("File written successfully."); }
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}

	
	/**
	 * Source: Apache POI Getting Started Examples
     * create a library of cell styles
     */
    private static Map<String, CellStyle> createStyles(Workbook wb){
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        DataFormat df = wb.createDataFormat();

        CellStyle style;
        Font headerFont = wb.createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(headerFont);
        styles.put("header", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(headerFont);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("header_date", style);

        Font font1 = wb.createFont();
        font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(font1);
        styles.put("cell_b", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font1);
        styles.put("cell_b_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(font1);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_b_date", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(font1);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_g", style);

        Font font2 = wb.createFont();
        font2.setColor(IndexedColors.BLUE.getIndex());
        font2.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(font2);
        styles.put("cell_bb", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(font1);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_bg", style);

        Font font3 = wb.createFont();
        font3.setFontHeightInPoints((short)14);
        font3.setColor(IndexedColors.DARK_BLUE.getIndex());
        font3.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(font3);
        style.setWrapText(true);
        styles.put("cell_h", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setWrapText(true);
        styles.put("cell_normal", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setWrapText(true);
        styles.put("cell_normal_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setWrapText(true);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_normal_date", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setIndention((short)1);
        style.setWrapText(true);
        styles.put("cell_indented", style);

        style = createBorderedStyle(wb);
        style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("cell_blue", style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }
}
