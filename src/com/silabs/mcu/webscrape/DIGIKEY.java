/**
 * Digikey.java
 *
 * Brad Stewart
 * Silicon Laboratorties, Inc.
 * MCU Marketing
 * 
 */
package com.silabs.mcu.webscrape;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.silabs.mcu.webscrape.io.ExcelFile;
import com.silabs.mcu.webscrape.io.SearchParameters;

/**
 * @author Brad Stewart (brstewar)
 * Implements the WebScraper interface. 
 * Initialized by InputProcessor, then uses Selenium web driver and Firefox to
 *  search Digikey (and Mouser) for product information and price data.
 *
 */
public class DIGIKEY implements WebScraper {
	
	  private HtmlUnitDriver driver;	  
	  private String baseUrl;
	  private SearchParameters sp;
	  private ArrayList<String> criteriaList;
	  private Integer priceCol, totalCols;
	  private Table<String,String,String> table;
	  private String currentQty;
	  
	  /**
	   * Sets up local data structures, web driver, and parses criteria
	   * @param sp		SearchParameters
	   */
	  public void initialize(SearchParameters sp) {
		this.sp = sp;
		if ( !sp.beQuiet() ) {System.out.print("Initializing WebDriver..."); }
		table = HashBasedTable.create(); // Data structure for storing scraped data
		priceCol = 0; // Speeds up the search for secondary quantity price data by remembering the column number of the price
//	    driver = new FirefoxDriver();
	    driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_17);
	    driver.setJavascriptEnabled(true);
	    baseUrl = "http://www.digikey.com/";
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);	    

		if ( !sp.beQuiet() ) {System.out.println("Done."); }
	  }
	  
	  /**
	   * Called by the input processor, parses input criteria and categories,
	   *  initiates data gathering, and writes the Excel file
	   */
	  public void scrape() {
		  writeTimeStampFile("Launch");
		  DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); // Date for the timestamp
		  Date date = new Date();
		  String criteriaString = sp.isParametric() ? sp.getCriteria().replaceAll(";", " .and. ") : sp.getCriteria().replaceAll(";"," .or. ");
		  criteriaList = new ArrayList<String>();
		  List<String> categoryList = Arrays.asList( sp.getCategory().split(";") );
		  processCriteria( criteriaString );
		  ExcelFile xls = new ExcelFile( sp );		  
	
		  int expectedResultCount = 0; // Keep track of Digikey's reported number of records for error checking
		  for ( int i=0; i< categoryList.size(); i++ ) {			  
			  String category = categoryList.get(i);
			  if ( !sp.beQuiet() ) { System.out.println("Searching "+category); }
			  category = category.trim();
			  buildHeaders(category, criteriaList.get(0));
			  int queryCount = 1;
			  
			  for ( String criteria : criteriaList ) {
				  if ( !sp.beQuiet() ) { System.out.println("  Query "+queryCount+" of "+criteriaList.size()+" -> "); }
				  try { 
					  expectedResultCount += searchDigikey( category, criteria ); 
					  }
				  catch (Exception e) { 
					  e.printStackTrace();
					  System.out.println("Search encountered error. Incomplete results will be written to file.");
				  }
				  queryCount++;
			  }
		  }
		  writeTimeStampFile("Digikey");
		  if ( sp.checkMouser() ) {    		    		    
			    try { searchMouser(); } 
			    catch (Exception e) { e.printStackTrace(); System.out.println("Error with MOUSER.com. Incomplete results will be written to file."); } 
			    writeTimeStampFile("Mouser");
		  }
		  
		  driver.quit();
		  String sheetName = xls.writeData(table, sp, dateFormat.format(date));
		  int actualResults = xls.getSheetNumerOfRows( sheetName );
		  if (expectedResultCount != actualResults) {
			  System.out.println("Error checking failed. Expected "+expectedResultCount+", but the file contains "+actualResults+".");
		  }
		  if ( sp.getXlsx() != null ) {
			  try { 
				 Table<String, String, String> diffs = Util.findPriceDiffs(table, xls);
				  xls.writePriceDiffTable( diffs, sp, "PriceChanges-"+dateFormat.format(date));
			  } catch (Exception e) {
				  e.printStackTrace();
				  System.out.println("Problem ecountered while searching for price changes.");
			  }
		  }
		  String outputFile = xls.writeFile();
		  if ( sp.isScheduled() ) {
				 Util.generateParamFile(sp, outputFile, sheetName);
			 }
		  writeTimeStampFile("Operation");
	  }
	  
	  /**
	   * Splits the search string into strings of 256 characters or less
	   * @param criteria
	   */
	  private void processCriteria(String criteria) {
		  if ( criteria.length() < 256 ) { criteriaList.add( criteria ); return; } // Base case
		  String s = criteria.substring(0, 256);
		  int i = s.contains(".or.") ? s.lastIndexOf(".or.") : s.lastIndexOf(".and.");
		  s = s.substring(0, i);
		  criteriaList.add ( s );
		  processCriteria( criteria.substring(i+4, criteria.length()) );
	  }

	  /**
	   * Controls the web driver and scrapes the data
	 * @return 
	   */
	  public int searchDigikey( String category, String criteria ) throws Exception {
		  
		    sendSearchParams( category, criteria );	
			if( !selectCategory(category) ) { return -1; }
		    enterQuantity("100");	
		    restrictPackaging();
		    String quantity = driver.findElement(By.cssSelector("#content > p:nth-child(2)")).getText();
		    quantity = quantity.substring( quantity.indexOf(":")+2, quantity.length());
		    if ( !sp.beQuiet() ) { System.out.print("    Processing "+quantity+" records... "); }	
			    String productTableSelector = "#productTable > tbody:nth-child(2) > tr"; // CSS Selector for Digikey's search result table	
			    int count = 0;
			    
		    count = scrapeTable(productTableSelector, count); // Build an attribute list for every part on the page
		    if ( !sp.beQuiet() ) { System.out.println(" Done."); }
			    // Re do the search for price data at a different quantity.
			    // Only the price column in captured during this run.
			    enterQuantity("1000");
			    restrictPackaging();
			if ( isElementPresent(By.partialLinkText("First")) ) {
				driver.findElement(By.partialLinkText("First")).click();
			}
			if ( !sp.beQuiet() ) { System.out.print("    Processing 1k unit pricing... "); }

		    scrapeSecondPrice(productTableSelector);
		    if ( !sp.beQuiet() ) { System.out.println(" Done."); }
		    
		    // Re do the search for price data at a different quantity.
		    // Only the price column in captured during this run.
		    enterQuantity("10000");
//		    restrictPackaging();
			if ( isElementPresent(By.partialLinkText("First")) ) {
				driver.findElement(By.partialLinkText("First")).click();
			}
			if ( !sp.beQuiet() ) { System.out.print("    Processing 10k unit pricing... "); }
		    scrapeSecondPrice(productTableSelector);
		    if ( !sp.beQuiet() ) { System.out.println(" Done."); }
		    
		    quantity = quantity.replace(",","");
		    return Integer.parseInt(quantity);
	  }
	  
	  private void writeTimeStampFile(String completedFunction) {
		  Date date = new Date();
		  DateFormat df = new SimpleDateFormat("dd-MMM-HH-mm-ss");
		  String timeStamp = df.format(date);
		  FileWriter writer = null;
	        try {
	            File logFile = new File(sp.getDirectory()+"\\timeLog.txt");

	            writer = new FileWriter(logFile, true);
	            writer.write(completedFunction+" completed at "+timeStamp+"\n");
	        } catch (Exception e) {
	        } finally {
	            try {
	                // Close the writer regardless of what happens...
	                writer.close();
	            } catch (Exception e) {
	            }
	        }
	  }
	/**
	 * Attempts to select the link of the specified Digikey category.
	 * @param category
	 * @return		true if succesful, false if not
	 */
	private boolean selectCategory(String category) {
		if ( isElementPresent(By.linkText( category )) ) { // Look for the specified category
			driver.findElement(By.linkText( category )).click();
			return true;
		} else if ( isElementPresent(By.className("seohtagbold"))
				&& driver.findElement(By.className("seohtagbold")).getText().contains( category ) ) {  // Do nothing
				return true;
		} else  {
			//if ( isElementPresent(By.className("catfilterlink")) ) { // If that category doesn't exist, click the first one
			return false;
		}
	}
	/**
	 * Builds a list of the data column headers
	 * Marks the original price column to be moved later
	 */
	private void buildHeaders(String category, String criteria) {
		sendSearchParams( category, criteria );
		if( !selectCategory(category) ) { return; }
			int i = 4; // Start with the 3rd column since we don't care about the picture, data sheet, etc
		    String head;
			while ( isElementPresent(By.cssSelector("#productTable > thead:nth-child(1) > tr:nth-child(1) > th:nth-child("+i+")")) ) { // Get the header names of the result table
				head = driver.findElement(By.cssSelector("#productTable > thead:nth-child(1) > tr:nth-child(1) > th:nth-child("+i+")")).getText();
				if ( head.contains("Price") ) { // We want to move the price data together later, so rename it here
					table.put("Header", "Price: Digikey @100", "Price: Digikey @100");
					table.put("Header", "Price: Digikey @1000", "Price: Digikey @1000");
					table.put("Header", "Price: Digikey @10000", "Price: Digikey @10000");
					priceCol = (Integer) i;
				} else {
			    	table.put("Header", head, head);
				}
		    	i++;
		    }		
		totalCols = i; 
	}
	
	/**
	 * Searches and retrieves all Digikey data for the returned search results.
	 * @param productTableSelector
	 * @param priceCol
	 * @param cols
	 */
	private int scrapeTable(String productTableSelector, int count) {
		
		driver.getCurrentUrl();
		List<WebElement> webTable = driver.findElements(By.cssSelector(productTableSelector)); // Get all web elements from the result table
	    for ( WebElement elem : webTable ) {
	    	count++;
	    	String rowPartNumber = elem.findElement(By.cssSelector("td:nth-child(4)")).getText(); // 3rd column is the Digikey part number
	    	
	    	for ( int i=4; i<totalCols; i++) {
	    		// nth-child selector into the web table containing the search results, Creates a compound selector based on the "productTableSelector"
    			String attribute = elem.findElement(By.cssSelector("td:nth-child("+i+")")).getText();  
    			String columnHeader = driver.findElement(By.cssSelector("#productTable > thead:nth-child(1) > tr:nth-child(1) > th:nth-child("+i+")")).getText();
    			
    			if (columnHeader.contains("Price") ) {
    					attribute = attribute.contains("@") ? attribute.substring(0, attribute.indexOf("@")) : "0"; // Clean up price strings by removing extra text
    					columnHeader = "Price: Digikey @100";
    			}
    			table.put(rowPartNumber, columnHeader, attribute);
	    	}
		}
	    
	    if ( !sp.beQuiet() && count%25==0) { System.out.print(" "+count+" "); }		    
	    // If there's multiple pages of results, go to the next one
	    if ( isElementPresent(By.linkText("Next")) ) {		    
	    	driver.findElement(By.linkText("Next")).click();
	    	scrapeTable(productTableSelector, count);
	    }    

	    return count;
	}

	/**
	 * Retrieves price data for another quantity.
	 * Code is very similar to above without the loop to retrieve every data column.
	 * @param productTableSelector
	 * @param priceCol
	 */
	private void scrapeSecondPrice(String productTableSelector) {
  
		driver.getCurrentUrl();
			List<WebElement> webTable = driver.findElements(By.cssSelector(productTableSelector));		
		    for ( WebElement elem : webTable ) {
		    	String rowPartNumber = elem.findElement(By.cssSelector("td:nth-child(4)")).getText();		    	
	    		String attribute = elem.findElement(By.cssSelector("td:nth-child("+priceCol+")")).getText();
    			String columnHeader = driver.findElement(By.cssSelector("#productTable > thead:nth-child(1) > tr:nth-child(1) > th:nth-child("+priceCol+")")).getText();
	
	    		if (columnHeader.contains("Price") ) {
					attribute = attribute.contains("@") ? attribute.substring(0, attribute.indexOf("@")) : "0"; // Clean up price strings by removing extra text
					columnHeader = "Price: Digikey @"+currentQty;
				}
				table.put(rowPartNumber, columnHeader, attribute);
			}
		     
		    if ( isElementPresent(By.linkText("Next")) ) {		    
		    	driver.findElement(By.linkText("Next")).click();
		    	scrapeSecondPrice(productTableSelector);
		    } 
	}
	
	/**
	 * Selects specific Digikey packaging types, primarily to eliminate duplicate results
	 *  from "DigiReel" packaging.
	 *  
	 * Selenium is an exception happy testing framework. The only way to check if an
	 *  option exists on the web page is to handle a NoSuchElementException. Since
	 *  different chips have different options, we can safely ignore these.
	 */
	public void restrictPackaging() {
		Select selection = null;
		String partNumber = null;
		try { 
				selection = new Select ( driver.findElement(By.name("pv7")) );
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.MILLISECONDS); // Don't wait for timeout to throw exception

				try { selection.selectByVisibleText("Bulk"); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }
				try { selection.selectByVisibleText("Cut Tape (CT)");  } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }
				try { selection.selectByVisibleText("*"); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }
				try { selection.selectByVisibleText("-"); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }  
				try { selection.selectByVisibleText("Tray");  } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ } 
				try { selection.selectByVisibleText("Tube"); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }  
				try { selection.selectByVisibleText("Tape & Reel (TR)"); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }  
				try { selection.selectByVisibleText("Tray - Waffle"); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ } 
				try { driver.findElement(By.cssSelector("form[name=\"attform\"] > input[type=\"submit\"]")).click(); } catch ( NoSuchElementException e ) { /* OK to ignore, element not always present */ }
			
			} 
			catch (NoSuchElementException e) {
				System.err.println("Unexpected search results. Could not find packaging filter.");
				try { 
					partNumber = driver.findElement(By.id("h1.seohtag")).getText();
					System.out.println("Unexpected results for "+partNumber+". No data captured.");}
				catch (NoSuchElementException e2) { }
			} 	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS); // Reset timeout limit	
	}

	
/**
 * Inputs the specified quantity into Digikey's quantity form and submits it
 * @param qty		String quantity to input to Digikey
 */
	private void enterQuantity(String qty) {
		currentQty = qty;
		try {
			driver.findElement(By.cssSelector("label > input[name=\"quantity\"]")).clear();
			driver.findElement(By.cssSelector("label > input[name=\"quantity\"]")).sendKeys(qty);
			driver.findElement(By.cssSelector("p > input[type=\"submit\"]")).click();
		} catch ( NoSuchElementException e) {
			System.err.println("Tried to put in quantity, but couldn't find it.");
		}
	}

/**
 * Enters the search criteria in Digikey's search box and executes the search.
 */
	private void sendSearchParams(String category, String criteria) {
		if ( sp.getParametricURL() != null ) {
			driver.get(sp.getParametricURL());
		} else {
			driver.get("http://www.digikey.com//");
			driver.findElement(By.id("search-input")).clear();
			driver.findElement(By.id("search-input")).sendKeys(criteria);
//			driver.findElement(By.id("rohs")).click();
			driver.findElement(By.id("btnSearch")).click();
		}
	}
	
/**
 *  Search Mouser for price data on each part number.
 *  Mouser does not support boolean search operators, so this runs a search for every
 *   individual part number. It takes forever.
 */
  public void searchMouser() throws Exception {
	  if ( !sp.beQuiet() ) { System.out.print("Cross searching MOUSER.com..."); }

	  table.put("Header", "Price: Mouser @100", "Price: Mouser @100"); // Add the column headers
	  table.put("Header", "Price: Mouser @1000", "Price: Mouser @1000");
	  table.put("Header", "Price: Mouser @10000", "Price: Mouser @10000");
	  
	  Set<String> rowPartNumbers = table.rowKeySet(); // Get the list of part numbers
	  int count = 0;	  
	  
	  for ( String rowPartNumber : rowPartNumbers ) {
		  if (rowPartNumber.equals("Header") ) { continue; } // Ignore the column header row.
		 count++;
	     String searchCriteria = table.get(rowPartNumber, "Manufacturer Part Number");
		 	try {
			    driver.get("http://www.mouser.com/"); // Go to mouser.com
			    driver.findElement(By.cssSelector("div input.search-txt")).click(); // Navigate to the Mouser search box
			    driver.findElement(By.cssSelector("div input.search-txt")).clear();
			    driver.findElement(By.cssSelector("div input.search-txt")).sendKeys(searchCriteria); // Enter the search criteria one by one
			    driver.findElement(By.id("ctl00_NavHeader_chkRoHSCompliantMouserHeader")).click(); 
			    driver.findElement(By.id("ctl00_NavHeader_btn1")).click(); // Click submit
		 	} catch (NoSuchElementException e) {
		 		System.out.println("Error encountered with "+ rowPartNumber);
		 		continue;
		 	}
		    
		    if( driver.getCurrentUrl().contains("ProductDetail") ) { // We're already on the page we want	    	
		    } else if ( driver.findElements(By.partialLinkText(searchCriteria)).size() > 0 ) { // If we get multiple search results, find the link for the product number we want
		    	 driver.findElement(By.partialLinkText(searchCriteria)).click(); // Click that link
		    } else if (driver.findElements(By.className("NRSearchMsg")) != null) { // If we get no results, keep going
		    	continue;
		    }

		    List<WebElement> elements = driver.findElements(By.cssSelector("td.PriceBreakQuantity")); // Get the web table with the price data 
		    String priceHundred = "0";
		    String priceThousand = "0";
		    String priceTenThousand = "0";
	
		    for ( WebElement elem : elements ) {
		    	
		    	if ( elem.getText().equals("100:") ) { // Find the 100 quantity label
		    		String id = elem.findElement(By.cssSelector("a")).getAttribute("id"); // Get its unique CSS ID
		    		id = id.replace("lnkQuantity", "lblPrice");	 // Modify the ID the point to the value 		
		    		priceHundred = driver.findElement(By.id(id)).getText().replace("$", ""); // Get the actual value and clean up the string		    		
		    	}
		    	
		    	if ( elem.getText().contains("1,") ) { // Mouser has a lot of 1,200 , 1,050 , 1,250, etc. quantities, so we want those too
		    		String id = elem.findElement(By.cssSelector("a")).getAttribute("id");
		    		id = id.replace("lnkQuantity", "lblPrice");	    		
		    		priceThousand = driver.findElement(By.id(id)).getText().replace("$", "");	    		
		    	}	
		    	
		    	if ( elem.getText().contains("10,") ) { 
		    		String id = elem.findElement(By.cssSelector("a")).getAttribute("id");
		    		id = id.replace("lnkQuantity", "lblPrice");	    		
		    		priceTenThousand = driver.findElement(By.id(id)).getText().replace("$", "");	    		
		    	}
		    }
		    
		    if ( !sp.beQuiet() && count%25==0) { System.out.print(" "+count+" "); }
		    
		    table.put(rowPartNumber, "Price: Mouser @100", priceHundred);
		    table.put(rowPartNumber, "Price: Mouser @1000", priceThousand);
		    table.put(rowPartNumber, "Price: Mouser @10000", priceTenThousand);
	  }
	 if ( !sp.beQuiet() ) { System.out.println(" Done ("+count+")."); }
  }  
	 	
/**
 * Auto-generated Selenium function to handle NoSuchElement exceptions.
 * I had mixed results for some reason, so it is not used everywhere. 
 * "By.id" generally does not work for dynamically generated pages.
 * @param by
 * @return
 */
  private boolean isElementPresent(By by) {
	  driver.manage().timeouts().implicitlyWait(5, TimeUnit.MILLISECONDS);
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    } finally {
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
  }  
}
