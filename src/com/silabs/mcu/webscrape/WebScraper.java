/**
 * 
 */
package com.silabs.mcu.webscrape;

import com.silabs.mcu.webscrape.io.SearchParameters;


/**
 * @author brstewar
 *
 */
public interface WebScraper {
	
	public void initialize( SearchParameters sp );
	
	public void scrape();
	
}
