/**
 * Util.java
 *
 * Brad Stewart
 * Silicon Laboratorties, Inc.
 * MCU Marketing
 * 
 */
package com.silabs.mcu.webscrape;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.silabs.mcu.webscrape.io.ExcelFile;
import com.silabs.mcu.webscrape.io.SearchParameters;

/**
 * This class contains static utility methods.
 * @author Brad Stewart brstewar
 *
 */
public class Util {
	
	/**
	 * Takes the Table data structure from a finished web scrape and compares it
	 *  with a Excel sheet containing data from a previous run. A new sheet will be
	 *  added to the workbook and contain only changed parts.
	 *  NOTE: This could be a lot more efficient if we're willing to modify the table.
	 * @param table		Table data structure with the changed parts: Table<Row, Column, Value>
	 * @param xls		Excel file containing old data
	 * @return			A new table containing only changed values
	 */
	public static Table<String, String, String> findPriceDiffs (Table<String, String, String> table, ExcelFile xls) {
		Table<String, String, String> diffTable = HashBasedTable.create();
		Set<String> newPartNumbers = table.rowKeySet();
		Workbook wb = xls.getWorkbook();
		if ( wb == null ) {
			System.err.println("WORKBOOK IS NULL");
		}
		Sheet oldDataSheet = xls.getWorkbook().getSheet(xls.getSp().getOldSheetName());
		if ( oldDataSheet == null ) {
			System.err.println("OLD DATA SHEET IS NULL");
		}
		System.out.println("OLD SHEET NAME "+xls.getSp().getOldSheetName());
		int dpnIndex = 0;
		Row excelHeaderRow = oldDataSheet.getRow(0);
		ArrayList<Integer> priceIndices = new ArrayList<Integer>();
		for (int i=0; i < excelHeaderRow.getPhysicalNumberOfCells(); i++ ) {
			if (excelHeaderRow.getCell(i).getStringCellValue().equals("Digi-Key Part Number") ) {
				dpnIndex = i; // Store the cell index of the Digikey Part Number to index the Table
			} if (excelHeaderRow.getCell(i).getStringCellValue().contains("Price") ) { // Get all the 
				priceIndices.add(i);
			}
		}

		for ( int rowNum = 1; rowNum < oldDataSheet.getPhysicalNumberOfRows(); rowNum++ ) { // Look for changes to products
			Row currentRow = oldDataSheet.getRow(rowNum);
			String currentPart = currentRow.getCell(0).getStringCellValue();
				
//				for ( int cellNum = 1; cellNum < currentRow.getPhysicalNumberOfCells(); cellNum++) {
				for ( Integer cellNum : priceIndices) {
					Cell currentCell = currentRow.getCell(cellNum);
					Double cellValue = 0.00;
					try {
						cellValue = currentCell.getNumericCellValue();
						} catch (IllegalStateException e) {
//							cellValue = Double.toString( currentCell.getNumericCellValue() );
						}
					int columnIndex = currentCell.getColumnIndex();
					String headerName = excelHeaderRow.getCell( columnIndex ).getStringCellValue(); 
					String attribute = table.get( currentRow.getCell(dpnIndex).getStringCellValue() , headerName );
					System.out.println("DEBUG: "+attribute);
					String manufacturer = table.get ( currentRow.getCell(dpnIndex).getStringCellValue(), "Manufacturer");
					
					if ( attribute == null ) {
						System.err.println("Null attribute for cellNum: "+cellNum + "colIndex: "+columnIndex);
					}
					Double attributeValue = Double.valueOf( attribute );
					if ( !cellValue.equals(attributeValue) && attributeValue != null ) {
						diffTable.put(currentPart, "NEW "+headerName, attribute);
						diffTable.put(currentPart, "OLD "+headerName, cellValue.toString());
						diffTable.put(currentPart, "Status", "Updated");
						diffTable.put(currentPart, "Manufacturer", manufacturer);
						diffTable.put(currentPart, "Manufacturer Part Number", currentPart);
					}
				}
		}
		
		for ( String partNumber : newPartNumbers ) { // Look for new products
			if ( partNumber.equals("Header") ) { continue; }
			boolean isNewPart = true;
			String currentPart = table.get( partNumber, "Manufacturer Part Number");
			for ( int rowNum = 1; rowNum < oldDataSheet.getPhysicalNumberOfRows(); rowNum++ ) {
				String oldPart = oldDataSheet.getRow(rowNum).getCell(0).getStringCellValue();
				if ( currentPart.equals(oldPart) ) { isNewPart = false; }
			}
			
			if ( isNewPart ) {
				diffTable.put(currentPart, "Status", "New Product");
				diffTable.put(currentPart, "Manufacturer", table.get(partNumber, "Manufacturer"));
				diffTable.put(currentPart, "Manufacturer Part Number", currentPart);
				Set<String> tableHeaderNames = table.columnKeySet();
				for ( String tableHeaderName : tableHeaderNames ) {
					if ( tableHeaderName.contains("Price")) {
						diffTable.put(currentPart, "NEW "+tableHeaderName, table.get(partNumber, tableHeaderName));
					}
				}
			}
		}
		
		return diffTable;
	}
	
	/**
	 * Generates a new params.txt file for scheduled tasks. It will
	 * automatically fill out the necessary fields to have have the script find
	 * changes between current-run data and previous-run data.
	 * @param sp
	 * @param currentFile
	 * @param currentSheet
	 */
	public static void generateParamFile(SearchParameters sp, String currentFile, String currentSheet) {
		StringBuilder flags = new StringBuilder();
		if ( sp.beQuiet() ) { flags.append(" -Q"); }
		if ( sp.checkMouser() ) { flags.append(" -M"); }
		if ( sp.isParametric() ) { flags.append(" -P"); }
		if ( sp.isScheduled() ) { flags.append(" -S"); }
		String output = "SEARCH: Digikey \n"+
						"FLAGS: "+flags.toString()+"\n"+
						"XLSX: "+currentFile+"\n"+
						"SHEETNAME: "+currentSheet+"\n"+
						"CATEGORY: "+sp.getCategory()+"\n"+
						"FOR: "+sp.getCriteria()+"\n"+
						"FILENAME: "+sp.getFilename()+"\n"+
						"DIRECTORY: "+sp.getDirectory()+"\n"+
						"END";
		
		
		 BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(sp.getDirectory()+"\\"+sp.getParamFilename()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 try {
			writer.write(output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
