:: Create tasks in Windows Task Scheduler for webscrape
::    - See http://ss64.com/nt/schtasks.html for more options.
::
:: ----------------------------------------------------------------------------
:: -------------------------- All Microcontrollers ----------------------------
:: ----------------------------------------------------------------------------
::
:: SCHEDULING PARAMETERS 
set "schedule=WEEKLY" :: [MINUTE,hourLY,DAILY,WEEKLY,MONTHLY]
set "day=MON,TUE" :: [MON,TUE,WED,THU,FRI,SAT,SUN]
set "hour=23:00" :: 11pm
::
:: FILE PARAMETERS 
set "jar=C:\MCU_Schedule\webscrape-1.0.2.jar" :: Location and name of webscrape.jar
set "params=C:\MCU_Schedule\" :: Folder containing params files NOT the file name
::
SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%silabs.txt"^
  /TN "webscrape\silabs"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%analogdevices.txt"^
  /TN "webscrape\analogdevices"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%atmel.txt"^
  /TN "webscrape\atmel"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%cypress.txt"^
  /TN "webscrape\cypress"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%freescale.txt"^
  /TN "webscrape\freescale"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%infineon.txt"^
  /TN "webscrape\infineon"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%microchip.txt"^
  /TN "webscrape\microchip"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%nuvoton.txt"^
  /TN "webscrape\nuvoton"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%nxp.txt"^
  /TN "webscrape\nxp"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%st.txt"^
  /TN "webscrape\st"

SCHTASKS /Create /SC %schedule% /D %day% /ST %hour%^
  /TR "\"java\" -jar %jar% %params%ti.txt"^
  /TN "webscrape\ti"
